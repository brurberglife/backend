CREATE TABLE Users (
  Username TEXT NOT NULL UNIQUE,
  GDPRCommitID TIMESTAMPTZ NOT NULL,
  ConsentedAt TIMESTAMPTZ DEFAULT Now()
);

CREATE TABLE Tasks (
  ID SERIAL NOT NULL UNIQUE,
  Text TEXT NOT NULL,
  Deadline TIMESTAMPTZ NOT NULL,
  Username TEXT references Users(Username),
  CreatedAt TIMESTAMPTZ DEFAULT Now()
);

CREATE TABLE TaskStatus (
  ID SERIAL NOT NULL UNIQUE,
  TaskID INT references Tasks(ID),
  StatusID INT references Statuses(ID),
  UpdatedAt TIMESTAMPTZ DEFAULT Now(),
  CreatedAt TIMESTAMPTZ DEFAULT Now()
);

CREATE TABLE Statuses (
  ID SERIAL NOT NULL UNIQUE,
  Text TEXT NOT NULL,
  CreatedAt TIMESTAMPTZ DEFAULT Now()
);

CREATE TABLE RecuringTasks (
  ID SERIAL NOT NULL UNIQUE,
  Text TEXT NOT NULL,
  Interval VARCHAR(10) NOT NULL,
  Deadline VARCHAR(10) NOT NULL,
  Username TEXT references Users(Username),
  CreatedAt TIMESTAMPTZ DEFAULT Now()
);

CREATE TABLE SleepTypes (
  ID SERIAL NOT NULL UNIQUE,
  Text TEXT NOT NULL UNIQUE,
  CreatedAt TIMESTAMPTZ DEFAULT Now()
);

CREATE TABLE SleepHabits (
  ID SERIAL NOT NULL UNIQUE,
  WakeUp TIMESTAMPTZ NOT NULL,
  BedTime TIMESTAMPTZ NOT NULL,
  Username TEXT references Users(Username),
  CreatedAt TIMESTAMPTZ DEFAULT Now()
);

CREATE TABLE SleepLogEntries (
  ID SERIAL NOT NULL UNIQUE,
  SleepTypeID INT NOT NULL references SleepTypes(ID),
  CreatedAt TIMESTAMPTZ DEFAULT Now()
);

CREATE TABLE MealTypes (
  ID SERIAL NOT NULL UNIQUE,
  Text TEXT NOT NULL UNIQUE,
  CreatedAt TIMESTAMPTZ DEFAULT Now()
);

CREATE TABLE DiningHabits (
  ID SERIAL NOT NULL UNIQUE,
  Text TEXT NOT NULL UNIQUE,
  CreatedAt TIMESTAMPTZ DEFAULT Now()
);

CREATE TABLE UserDiningHabits (
  ID SERIAL NOT NULL UNIQUE,
  DiningHabitID INT NOT NULL references DiningHabits(ID),
  Username TEXT references Users(Username),
  CreatedAt TIMESTAMPTZ DEFAULT Now()
);

CREATE TABLE DiningLogEntries (
  ID SERIAL NOT NULL UNIQUE,
  MealID BIGINT NOT NULL references Meals(ID),
  Username TEXT references Users(Username),
  CreatedAt TIMESTAMPTZ DEFAULT Now()
);

CREATE TABLE FoodItems (
  ID SERIAL NOT NULL UNIQUE,
  Text TEXT NOT NULL,
  CreatedAt TIMESTAMPTZ DEFAULT Now()
);

CREATE TABLE Meals (
  ID BIGSERIAL NOT NULL UNIQUE,
  CommentText TEXT NOT NULL,
  FoodItemID BIGINT NOT NULL references FoodItems(ID),
  Username TEXT references Users(Username),
  Favorited BOOLEAN NOT NULL DEFAULT FALSE,
  CreatedAt TIMESTAMPTZ DEFAULT Now()
);
